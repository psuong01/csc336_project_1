#Contribution
#Satya Gupta:
#Worked on creating the high level program
#Porrith, Dramane and I together worked on the sql statements

#Dramane Diakite

#Azizur Rahman
#Built the ER Diagram which was used to draw flowchart by Porrith

#Porrith Suong

import MySQLdb
import MySQLdb.cursors
import json
#MySQLdb Notes:
#MySQLdb.cursors.DictCursor is used to output select statements in dictionary
#.execute(sql) executes sql statements
#to update or alter sql tables .execute(sql) is to be followed with .commit()


#connect to the database replace user and password with your credentials
db = MySQLdb.connect(host="localhost",user="F16336user",passwd="password",db="F16336team7",cursorclass=MySQLdb.cursors.DictCursor)

cursor = db.cursor()

#select all attributes from table discounts where disc is not CUSTOMER DISCOUNT and INITIAL CUSTOMER
sql = "select * from discounts where discounttype <> 'CUSTOMER DISCOUNT' and discounttype <> 'INITIAL CUSTOMER' and lowqty is not NULL"
cursor.execute(sql)

#fetch the attributes and store it in discounts as a dictionary
discounts = cursor.fetchall()

#create empty list salesdetail
salesdetail = []
for i in discounts:
    #select title_id and price from titles, stor_id and sum of qty with same title_id from salesdetail (INNER JOIN)
	sql = "select titles.title_id, price, stor_id, sum(qty) as totalqty from salesdetail, titles where titles.title_id=salesdetail.title_id and stor_id like " + str(i['stor_id']) + " group by title_id"
	cursor.execute(sql)
    #if highqty from discount table is NULL assign a really large value to potentially ignore upperlimit
	if i['highqty'] is None:
		i['highqty'] = 20000
	temp = []
	for item in cursor.fetchall():
        #Since Bookstores order titles in high volume they would get higher discount which helps them earn a revenue out of that revenue we have to cut the royalty fees for authors to achieve net revenue
        #if the total number of sales for a particular id (sum(qty)) fall within the discounttype range apply that discount value to the sales
            if (float(item['totalqty'])>= i['lowqty'] and float(item['totalqty']) <= i['highqty']):
			item['price'] = float(item['price'])
			item['discount'] = i['discount']
			# Update the discount attribute in salesdetails with the new discount computed
			sql = "update salesdetail set discount = " + str(i['discount']) + " where stor_id like '" + str(item['stor_id']) + "' and title_id like '" + str(item['title_id'] + "'")
			cursor.execute(sql)
			db.commit()
			item['totalqty'] = float(item['totalqty'])
			temp += [{'net_income': (100-item['discount'])*(item['price'] * item['totalqty'])/100.0 , 'title_id': item['title_id']}]

	salesdetail += temp

#add column royalty_earned in table roysched if it doesn't exist
sql = "alter table roysched add royalty_earned float"

try:
    cursor.execute(sql)
except MySQLdb.Error as err:
    print "Duplicate found!\n"

db.commit()
for sale in salesdetail:
	sql = "select royalty from roysched where title_id like '"+sale['title_id']+"'"
	cursor.execute(sql)
	sale['royalty'] = cursor.fetchall()[0]['royalty']*sale['net_income']/100.0
	sale['net_revenue'] = sale['net_income'] - sale['royalty']
    # Update the royalty_earned attribute in roysched with the new royalty_earned computed
	sql = "update roysched set royalty_earned = " + str(sale['royalty']) + " where title_id like '" + str(sale['title_id']) + "'"
	cursor.execute(sql)
	db.commit()

#For testing purposes
print(json.dumps(salesdetail, indent=4))
#close db when done
db.close()
